<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionresponse extends Model
{
    protected $guarded = [];

    public function examination()
    {
        return $this->belongsTo('App\Examination');
    }
    
    public function questions()
    {
        return $this->hasMany('App\Question','id','question_id');
    }
    
    public function choices()
    {
        return $this->hasMany('App\Choice','id','choice_id');
    }
}
