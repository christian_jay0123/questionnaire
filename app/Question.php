<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Question extends Model
{
    use softDeletes;
    protected $guarded = [];
    
    public function questionnaire()
    {
        return $this->belongsTo('App\Questionnaire');
    }
    
    public function choices()
    {
        return $this->hasMany('App\Choice');
    }

    public function responses()
    {
        return $this->hasMany('App\Questionresponse');
    }

}
