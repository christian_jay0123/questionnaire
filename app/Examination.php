<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    //
    protected $guarded = [];
    public function questionnaire()
    {
        return $this->belongsTo('App\Questionnaire');
    }

    public function questionresponses()
    {
        return $this->hasMany('App\Questionresponse');
    }
}
