<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questionnaire extends Model
{
    //
    use SoftDeletes;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function examinations()
    {
        return $this->hasMany('App\Examination');
    }
}
