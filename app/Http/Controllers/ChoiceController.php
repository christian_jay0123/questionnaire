<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Choice;
class ChoiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         abort(404,'Unauthorized action.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($questionnaire_id,$question_id)
    {
        $question = Question::findOrFail($question_id);
        $choice = new Choice();
        return view('questionnaire.question.choice.create',compact('questionnaire_id','question','choice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$questionnaire_id,$question_id)
    {
        $data = $this->validateChoice($request);
        $data['question_id'] = $question_id;
        $choice = Choice::create($data);
        return redirect('/questionnaire/'.$questionnaire_id.'/question/'.$question_id.'/choice/'.$choice->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire_id,$question_id,$choice_id)
    {

        $choice = Choice::findOrFail($choice_id);
        return view('questionnaire.question.choice.show',compact('choice','questionnaire_id','question_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($questionnaire_id,$question_id,$choice_id)
    {
        
        $choice = Choice::where('id',$choice_id)->where('question_id',$question_id)->first();
        
        return view('questionnaire.question.choice.edit',compact('choice','questionnaire_id','question_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $questionnaire_id,$question_id,$choice_id)
    {
        $choice = Choice::findOrFail($choice_id)->update($this->validateChoice($request));

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($questionnaire_id,$question_id,$choice_id)
    {
        Choice::findOrFail($choice_id)->delete();
        return redirect('/questionnaire/'.$questionnaire_id.'/question');
    }

    public function validateChoice($request)
    {
        return $request->validate([
            'choice'=>'required',
        ]);
    }

}
