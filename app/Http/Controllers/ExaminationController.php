<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
class ExaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire_id)
    {
        $questionnaire = Questionnaire::findOrFail($questionnaire_id);
        return view('questionnaire.examination.index',compact('questionnaire'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$questionnaire_id)
    {
        $data = $this->validateExamination($request);
        $data['examination']['ip_address'] = $request->ip();
        $examination = Questionnaire::findOrFail($questionnaire_id)->examinations()->create($data['examination']);
        $examination->questionresponses()->createMany($data['responses']);
        return 'thank you for answering this questionnaire, kindly make sure the contact number that you insert is active. Your score will be sent. after we review your answers.';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function validateExamination($request)
    {
        
        return $request->validate([
            'responses.*.choice_id' =>'required',
            'responses.*.question_id' =>'required',
            'examination.name'=>'required',
            'examination.contact_no'=>'required',
        ]);
    }
}
