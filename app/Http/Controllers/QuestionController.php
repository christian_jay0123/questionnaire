<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;
use App\Choice;
class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire_id)
    {
        
        $questionnaire = Questionnaire::findOrFail($questionnaire_id)->load('questions.choices');
        // return $questionnaire;
        return view('questionnaire.question.index',compact('questionnaire'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($questionnaire_id)
    {
        $questions = new Question();
        return view('questionnaire.question.create',compact('questionnaire_id','questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$questionnaire_id)
    {
        $data = $this->validateQuestion($request);
        $data['questionnaire_id'] = $questionnaire_id;
        $question = Question::create($data);
        

        return redirect('/questionnaire/'.$questionnaire_id.'/question/'.$question->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire_id,$question_id)
    {
        $question = Question::findOrFail($question_id);
        // return $question->load('choices');
        return view('questionnaire.question.show',compact('question','questionnaire_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($questionnaire_id,$question)
    {
        $questions = Question::findOrFail($question);
        return view('questionnaire.question.edit',compact('questions','questionnaire_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $questionnaire_id,$question)
    {
        Question::findOrFail($question)->update($this->validateQuestion($request));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($questionnaire_id,$question)
    {
        $question = Question::findOrFail($question);
        $question->delete();
        return redirect('questionnaire/'.$questionnaire_id.'/question');
    }

    public function validateQuestion($request)
    {
        return $request->validate([
                'question'=>'required|min:5',
            ]);
    }
}
