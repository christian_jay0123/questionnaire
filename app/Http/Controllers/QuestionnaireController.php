<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use Auth;
class QuestionnaireController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questionnaires = Questionnaire::all();

        return view('questionnaire.index',compact('questionnaires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questionnaire = new Questionnaire();
        return view('questionnaire.create',compact('questionnaire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validateQuestionnaire($request);
        $data['user_id'] = Auth::User()->id;
        $question = Questionnaire::create($data);

        return redirect('questionnaire/' . $question->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        return view('questionnaire.show',compact('questionnaire'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $questionnaire = Questionnaire::findOrFail($id);
        return view('questionnaire.edit',compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire->update($this->validateQuestionnaire($request));
        return redirect('questionnaire/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaireId =  Questionnaire::findOrFail($id);
        $questionnaireId->delete();
        return redirect('questionnaire');
    }

    public function validateQuestionnaire($request)
    {
        return $request->validate([
            'title'=>'min:5',
            'purpose'=>'min:5',
        ]);
    }
}
