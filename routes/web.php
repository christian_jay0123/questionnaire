<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('questionnaire','QuestionnaireController');
Route::resource('questionnaire/{questionnaire_id}/result','ResultController');
Route::resource('questionnaire/{questionnaire_id}/question','QuestionController');
Route::resource('questionnaire/{questionnaire_id}/examination','ExaminationController');
Route::resource('questionnaire/{questionnaire_id}/question/{question_id}/choice','ChoiceController');
Route::get('/home', 'HomeController@index')->name('home');
