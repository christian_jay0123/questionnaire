@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@auth<a href="/questionnaire"><</a> Create Questionnaire @endauth</div>
                <div class="card-body">
                    <form action="/questionnaire" method="POST">
                        @include('questionnaire.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection