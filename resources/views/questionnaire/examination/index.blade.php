@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form method="POST" action="/questionnaire/{{$questionnaire->id}}/examination">
                    @include('questionnaire.examination.form')
                </form>
            </div>
        </div>
    </div>
</div>
@endsection