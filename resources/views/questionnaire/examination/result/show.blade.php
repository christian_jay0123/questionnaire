@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire/{{$questionnaire_id}}/result">< </a>
                    <div class="d-flex justify-content-end">
                    </div>
                </div>
                
                <div class="card-body">
                    @foreach($examinations->questionresponses as $response)
                            {{$response->questions[0]->question}}
                            <ul class="group-list">
                                <li>
                                    {{$response->choices[0]->choice}}
                                </li>          
                            </ul>
                    @endforeach
                </div>
                <div class="card-footer">
                    <center>
                        <form method="POST" action="#">
                            <div>
                                <label for="score">
                                    Score
                                    <input type="text" style="text-align:center;" id="score" class="form-control col-xl-4"/>
                                </label>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-secondary col-xl-2 ">Submit</button>
                            </div>
                        </form>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection