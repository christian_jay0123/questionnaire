@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="/questionnaire/{{$examinees->id}}"><</a>
                </div>
                <div class="card-body">
                    @forelse($examinees->examinations as $examinee)
                        <ul>
                            <a href="result/{{$examinee->id}}">{{$examinee->name}}</a>
                            <li>IP ADDRESS: {{$examinee->ip_address}}</li>
                            <li>Contact Num: {{$examinee->contact_no}}</li>
                            <li>Date : {{$examinee->created_at}}</li>
                        </ul>
                        @empty
                            No Choice Found.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection