<div class="card-header">
    <a href="/questionnaire/{{$questionnaire->id}}"><</a>{{$questionnaire->title}}
</div>
    <div class="card-body">
        
        @foreach($questionnaire->questions as $key => $question)
        @error('responses.'.$key.'.choice_id')<div><small style="color:red">{{$message}}</small></div>@enderror
        {{$key + 1}}. {{$question->question}}
        <ul class="list-group">
            @forelse($question->choices as $choice)
                <label for="choice{{$choice->id}}">
                    <li class="list-group-item">
                        <input type="radio" class="mr-2" name="responses[{{$key}}][choice_id]" 
                        {{(old('responses.'.$key.'.choice_id') == $choice->id)? 'checked' : ''}} id="choice{{$choice->id}}" value="{{$choice->id}}"/>
                        {{$choice->choice}}
                        <input type="hidden" name="responses[{{$key}}][question_id]"
                        value="{{$question->id}}" />
                    </li>
                </label>
            @empty
                No Choice Found. [contact the management]
            @endforelse
        </ul>
        @endforeach
    </div>
    <input type="text" placeholder="insert your name" name="examination[name]" class="form-control" value="{{ old('name')}}">
        @error('name')<div><small style="color:red">{{$message}}</small></div>@enderror
    <input type="text" placeholder="insert your contact number" name="examination[contact_no]" class="form-control" value="{{ old('contact_no')}}">
    @error('contact_no')<div><small style="color:red">{{$message}}</small></div>@enderror
<div class="card-footer">
    <button type="submit" class="btn btn-dark">Submit</a>
</div>
@csrf