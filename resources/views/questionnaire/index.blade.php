@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="home"><</a></div>
                @auth
                <div class="card-body">
                   <div class="card-header"><a href="questionnaire/create" class="btn btn-dark">Create Questionnaire</a></div>
                </div>
                @endauth
            </div>
        </div>
    </div>
    @forelse($questionnaires as $questionnaire)
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="questionnaire/{{$questionnaire->id}}">{{$questionnaire->title}}</a></div>
                <div class="card-body">
                    {{$questionnaire->purpose}}
                </div>
                <div class="card-footer d-flex"><div class="p-2 w-100"><small>Number of examinee:[{{$questionnaire->examinations->count()}}]</small></div><div class="text-right flex-row-reverse"><small><strong>Created by {{$questionnaire->user->name}} </strong></small></div></div>
            </div>
        </div>
    </div>
    @empty
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">There is no question.</div>
            </div>
        </div>
    </div>
    @endforelse
</div>
@endsection