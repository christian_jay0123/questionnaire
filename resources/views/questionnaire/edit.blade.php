@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="/questionnaire/{{$questionnaire->id}}"><</a>
                    Update Questionnaire
                </div>
                <div class="card-body">
                    <form action="/questionnaire/{{$questionnaire->id}}" method="POST">
                        @method('PATCH')
                        @include('questionnaire.form')
                    </form>

                    <form method="POST" action="/questionnaire/{{$questionnaire->id}}">
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection