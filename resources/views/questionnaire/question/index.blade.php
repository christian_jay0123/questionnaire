@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="/questionnaire/{{$questionnaire->id}}"><</a>{{$questionnaire->title}}
                </div>
                @foreach($questionnaire->questions as $question)
                <div class="card-body">
                    <a href="/questionnaire/{{$questionnaire->id}}/question/{{$question->id}}">{{$question->question}}</a>
                    <ul>
                        @forelse($question->choices as $choice)
                        <li><a href="question/{{$question->id}}/choice/{{$choice->id}}"> {{$choice->choice}} </a></li>
                        @empty
                        No Choice Found.
                        @endforelse
                    </ul>
                </div>
                @endforeach
                <div class="card-footer">
                    <a href="question/create" class="btn btn-dark">Create Question</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection