@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire/{{$questionnaire_id}}/question"><</a> Create Question</div>
                <div class="card-body">
                    <form action="/questionnaire/{{$questionnaire_id}}/question" method="POST">
                        @include('questionnaire.question.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection