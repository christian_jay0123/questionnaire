<div>
    <input type="text" name="choice" class="form-control" value="{{ old('choice') ?? $choice->choice }}">
    @error('choice')<div><small style="color:red">{{$message}}</small></div>@enderror
    <small>Anything you want.</small>
</div>

<div class="d-flex justify-content-end">
    <button type="submit" class="btn btn-primary">Save</button>
</div>
@csrf