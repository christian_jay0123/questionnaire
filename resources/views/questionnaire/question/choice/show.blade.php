@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire/{{$questionnaire_id}}/question">< </a>{{$choice->choice}}
                    <div class="d-flex justify-content-end">
                        <a href="{{$choice->id}}/edit">Edit</a>
                    </div>
                </div>
                <div class="card-body">
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection