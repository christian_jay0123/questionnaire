@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire/{{$questionnaire_id}}/question"><</a>{{$question->question}}
                    <div class="d-flex justify-content-end">
                        <a href="{{$question->id}}/edit">Edit</a>
                    </div>
                </div>
                <div class="card-body">
                </div>
                <div class="card-footer">
                    <a href="{{$question->id}}/choice/create" class="btn btn-dark">Create Answer</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection