<div>
    <input type="text" name="question" class="form-control" value="{{ old('question') ?? $questions->question}}">
    @error('question')<div><small style="color:red">{{$message}}</small></div>@enderror
    <small>The question must be specific.</small>
</div>

<div class="d-flex justify-content-end">
    <button type="submit" class="btn btn-primary">Save</button>
</div>
@csrf