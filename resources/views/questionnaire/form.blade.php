<div>
    <input type="text" name="title" class="form-control" value="{{ old('title') ?? $questionnaire->title}}">
    @error('title')<div><small style="color:red">{{$message}}</small></div>@enderror
    <small>The questionnaire must consist of title</small>
    
</div>
<div>
    <input type="text" name="purpose" class="form-control" value="{{old("purpose") ?? $questionnaire->purpose}}">
    @error('purpose')<div><small style="color:red">{{$message}}</small></div>@enderror
    <small>The questionnaire must consist of purpose</small>
</div>
<div  class="d-flex justify-content-end">
<button type="submit" class="btn btn-primary">Save</button>
</div>
@csrf