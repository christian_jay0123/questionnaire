@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire"><</a>{{$questionnaire->title}}
                    <div class="d-flex justify-content-end">
                        @auth
                        <a href="{{$questionnaire->id}}/edit">Edit</a>
                        @endauth
                    </div>
                </div>
                <div class="card-body">
                    {{$questionnaire->purpose}}
                </div>
                <div class="card-footer">
                    @auth
                        <a href="{{$questionnaire->id}}/question" class="btn btn-dark">Question List</a>
                        <a href="{{$questionnaire->id}}/result" class="btn btn-dark">Examinee's Result</a>
                    @else
                        <a href="{{$questionnaire->id}}/examination" class="btn btn-dark">Take Exam</a>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection