@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="/questionnaire" class="btn btn-dark">Questionnaire</a></div>

                <div class="card-body">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
